<?php
/**
 * Default site sharer widget
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');

class SharerWidget extends Widget{
	public function init(){
		$this->setInfo(array(
			'component' => 'sharer',
			'type' => 'block',
			'name' => 'sharer',
			'title' => 'Facebook, Twitter, LinkedId, Email, Instagram',
			'description' => '',
			'wireframe' => 'sharer',
			'icon' => 'sharer',
			'saveoptions' => array(
				'facebook','twitter','linkedin','instagram','email','sharetext','displaywidgettitle','introtext'
			)
		));
	}

	public function render(){
		global $service;
		$service->get('Ressource')->get('core/display/form');
		$service->get('Ressource')->get('core/display/form/field');
		$url = $service->get('Url')->get();
		$url = $url['language'].'/'.$url['path'];
		$form = new Form('', 'share_email_dialog_form', '', $method='POST');
		$form->add(new TextFormField('email','',array(
			'placeholder'=>SHARER_SHARETHISPAGEVIAEMAIL_EMAIL,
			'title'=>_EMAIL,
			'id' => 'email'
			)));
		$form->add(new TextFormField('ename','',array(
			'placeholder'=>_NAME,
			'title'=>SHARER_SHARETHISPAGEVIAEMAIL_NAME,
			'id' => 'ename'
		)));
		$form->add(new TextFormField('subject',$this->data->getVar('widget_options')['sharetext'],array(
			'placeholder'=>_SUBJECT,
			'title'=>SHARER_SHARETHISPAGEVIAEMAIL_SUBJECT,
			'id' => 'subject'
		)));
		$form->add(new TextareaFormField('message',$this->data->getVar('widget_options')['introtext']."\n\n".URL.$url,array(
			'placeholder'=>_MESSAGE,
			'title'=>SHARER_SHARETHISPAGEVIAEMAIL_MESSAGE,
			'id' => 'message'
		)));

		$cttitle = $service->get('Content')->get()->getVar('content_title');
		$ctdesc = $service->get('Content')->get()->getVar('content_exerpt');
		$code = '<div class="sharer">';
		if ($this->data->getVar('widget_options')['email']){
			$code .= '
			<div class="modal fade" id="sharer_email_dialog" tabindex="-1" role="dialog" aria-labelledby="sharer_email_dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="label">'.SHARER_SHARETHISPAGEVIAEMAIL.'</h4>
						</div>
						<div class="modal-body">
							<h3>'.SHARER_SHARETHISPAGEVIAEMAIL.'</h3>
							'.$form->render().'
							<div class="modal-body-msg"></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" id="cancel">'._CANCEL.'</button>
							<button type="button" class="btn btn-primary" id="ok">'._SEND.'</button>
						</div>
					</div>
				</div>
			</div>';
			$service->get('Ressource')->addScript('
			function setupSharerDialog(){
				$("#sharer_email_dialog #ok").click(function(e) {
					e.preventDefault();
					$.ajax({
						type: "POST",
						url: "'.URL.$service->get('Language')->getCode().'/api/sharer/controller/sharer/sendmail",
						data: {
							email: $("#sharer_email_dialog #email").val(),
							ename: $("#sharer_email_dialog #ename").val(),
							subject: $("#sharer_email_dialog #subject").val(),
							message: $("#sharer_email_dialog #message").val()
						},
						success: function success(data, textStatus, jqXHR){
							$("#sharer_email_dialog .modal-body-form").hide();
							$("#sharer_email_dialog .modal-body-msg").show();
							if (data.success) {
								$("#sharer_email_dialog #ok").hide();
								$("#sharer_email_dialog #cancel").hide();
								$("#sharer_email_dialog .modal-body-msg").html("'.addslashes(SHARER_SHARETHISPAGEVIAEMAIL_SENT).'");
								setTimeout(\'$("#sharer_email_dialog").modal("hide")\',2000);
							}else {
								$("#sharer_email_dialog .modal-body-msg").html("'.addslashes(SHARER_SHARETHISPAGEVIAEMAIL_NOTSENT).'");
								setTimeout(\'$("#sharer_email_dialog").modal("hide")\',2000);
							}
						},
						error: function success(data, textStatus, jqXHR){
							$("#sharer_email_dialog .modal-body-form").hide();
							$("#sharer_email_dialog .modal-body-msg").show();
							$("#sharer_email_dialog .modal-body-msg").html("'.addslashes(SHARER_SHARETHISPAGEVIAEMAIL_NOTSENT).'");
							setTimeout(\'$("#sharer_email_dialog").modal("hide")\',2000);
						}
					});

				});
			}
			setupSharerDialog();
			');
		}
		if ($this->data->getVar('widget_options')['facebook']){
			$service->get('Ressource')->addScript('
			function showSharerFBDialog(){
				$("#sharer_facebook_dialog").bind("show", function() {
					$("#sharer_facebook_dialog #cancel").click(function(e) {
						e.preventDefault();
					});
				});
				$("#sharer_facebook_dialog").bind("hide", function() {
					$("#sharer_facebook_dialog a.btn").unbind();
				});
				$("#sharer_facebook_dialog").modal({
				   "backdrop" : "static",
				   "keyboard" : true,
				   "show" : true
				});
			}');
		}
        $popup = ' onclick="window.open(this.href,\'targetWindow\',\'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=650,height=650\');return false;"';
		if ($this->data->getVar('widget_options')['email']){
			$code .= '<a href="#" class="email" data-toggle="modal" data-target="#sharer_email_dialog">';
			$code .= '</a>';
		}
		if ($this->data->getVar('widget_options')['twitter']){
			$code .= '<a href="https://twitter.com/share?text='.urlencode($cttitle.":\n".$ctdesc).'&url='.urlencode(URL.$url).'" class="twitter"'.$popup.'>';
			$code .= '</a>';
		}
		if ($this->data->getVar('widget_options')['facebook']){
			$code .= '<a href="https://www.facebook.com/sharer.php?t='.urlencode($cttitle).'&u='.urlencode(((substr(URL,0,2) == '//')?'http:':'').URL.$url).'" class="facebook"'.$popup.'>';
			$code .= '</a>';
		}
		if ($this->data->getVar('widget_options')['linkedin']){
			$code .= '<a href="https://www.linkedin.com/shareArticle?mini=true&url='.urlencode(URL.$url).'&title='.urlencode($cttitle).'&summary='.urlencode($ctdesc).'&source='.urlencode($service->get('Setting')->get('sitename')).'" class="linkedin"'.$popup.'>';
			$code .= '</a>';
		}
		if ($this->data->getVar('widget_options')['instagram']){
			$code .= '<span class="ig-follow" data-id="5479dee" data-count="true" data-size="large" data-username="true"></span>';
			$service->get('Ressource')->addScript('(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.src="http://instagramfollowbutton.com/components/instagram/v2/js/ig-follow.js";s.parentNode.insertBefore(g,s);}(document,"script"));');
		}
		$code .= '</div>';
		return $code;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$form->add(new YesnoFormField('facebook',$options['facebook'],array(
			'tab'=> 'basic',
			'title' => 'Facebook',
			'width' => 3
		)));
		$form->add(new YesnoFormField('twitter',$options['twitter'],array(
			'tab'=> 'basic',
			'title' => 'Twitter',
			'width' => 3
		)));
		$form->add(new YesnoFormField('linkedin',$options['linkedin'],array(
			'tab'=> 'basic',
			'title' => 'Linked In',
			'width' => 3
		)));
		$form->add(new YesnoFormField('instagram',$options['instagram'],array(
			'tab'=> 'basic',
			'title' => 'Instagram',
			'width' => 3
		)));
		$form->add(new YesnoFormField('email',$options['email'],array(
			'tab'=> 'basic',
			'title' => _EMAIL,
		)));
		$form->add(new TextFormField('sharetext',$options['sharetext'],array(
			'tab'=> 'basic',
			'lang'=> $defaultlang['code'],
			'title' => 'Texte de partage',
			'translations' => $form->getTranslations($objs,'widget_options','sharetext')
		)));
		$form->add(new TextareaFormField('introtext',$options['introtext'],array(
			'tab'=> 'basic',
			'lang'=> $defaultlang['code'],
			'title' => 'Introduction du courriel',
			'translations' => $form->getTranslations($objs,'widget_options','introtext')
		)));
		return $form;
	}
}
?>
