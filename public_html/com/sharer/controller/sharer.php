<?php
/**
 * Sharer controller
 *
 * Handles sharer requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/protocol/email');

class SharerController extends Controller{
	/**
	 * Initializes the controller.
	 *
	 * For a controller to find its associated view object in a component
	 * folder, you need to add this line to the init method:
	 * $this->component = 'component folder name here';
	 *
	 * @access public
	 */
	function init(){
		$this->component = 'sharer';
	}
	
	public function execute(){
	}
	
	public function apiCall($op){
		global $service;
		$ret = array();
		
		if ($op == 'sendmail') {
			$mail = new Email();
			$mail->to($request['email']);
			$mail->from($service->get('Setting')->get('adminmail'));
			$mail->subject($request['subject']);
			$mail->body(SHARER_SHARETHISPAGEVIAEMAIL_FROM.$request['ename']."\n\n".$request['message']);
			$mail->send();
			$ret['success'] = 1;
			return $ret;			
		}
	}
	
	public function hasAccess($op){
		return true;
	}
}
?>